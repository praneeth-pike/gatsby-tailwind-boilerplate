module.exports = {
  siteMetadata: {
    title: 'Gatsby Default Starter',
  },
  plugins: [
    {
      resolve: 'gatsby-plugin-sass',
      options: {
        precision: 8,
      },
    },
    'gatsby-plugin-react-helmet'
  ],
}
